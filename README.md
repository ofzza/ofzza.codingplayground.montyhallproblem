# Monty Hall problem (Simulation)

Having found myself in a position of trying to convince someone who simply wouldn�t be convinced, I made a simple simulation that should allow anyone to get an intuitive feel for the Monty Hall problem.
The simulation allows for both manual and automated playing, and even allows you to cheat and see what is behind each door before making your choice.