﻿namespace ofzza.MontyHallProblem {
    partial class mainForm {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(mainForm));
            this.modeSelectPanel = new System.Windows.Forms.Panel();
            this.descriptionText = new System.Windows.Forms.TextBox();
            this.simulationPanel = new System.Windows.Forms.Panel();
            this.SelectionPanel = new System.Windows.Forms.Panel();
            this.StatusPanel = new System.Windows.Forms.Panel();
            this.switchSelectionRadio = new System.Windows.Forms.RadioButton();
            this.persistantSelectionRadio = new System.Windows.Forms.RadioButton();
            this.randomSelectionRadio = new System.Windows.Forms.RadioButton();
            this.StatusTextbox = new System.Windows.Forms.TextBox();
            this.DoorsPanel = new System.Windows.Forms.Panel();
            this.Door2PrizePanel = new System.Windows.Forms.Panel();
            this.Door2Panel = new System.Windows.Forms.Panel();
            this.Door2SelectedPanel = new System.Windows.Forms.Panel();
            this.Door1PrizePanel = new System.Windows.Forms.Panel();
            this.Door1Panel = new System.Windows.Forms.Panel();
            this.Door1SelectedPanel = new System.Windows.Forms.Panel();
            this.Door3PrizePanel = new System.Windows.Forms.Panel();
            this.Door3Panel = new System.Windows.Forms.Panel();
            this.Door3SelectedPanel = new System.Windows.Forms.Panel();
            this.AutoSelectPanel = new System.Windows.Forms.Panel();
            this.cheatCheck = new System.Windows.Forms.CheckBox();
            this.speedLabel = new System.Windows.Forms.Label();
            this.speedBar = new System.Windows.Forms.TrackBar();
            this.autoSelectCheckbox = new System.Windows.Forms.CheckBox();
            this.StatisticsPanel = new System.Windows.Forms.Panel();
            this.InnerStatsPanel = new System.Windows.Forms.Panel();
            this.LosesPanel = new System.Windows.Forms.Panel();
            this.FirstTimeMissesCountLabel = new System.Windows.Forms.Label();
            this.FirstTimeMissesLabel = new System.Windows.Forms.Label();
            this.losesCountLabel = new System.Windows.Forms.Label();
            this.losesLabel = new System.Windows.Forms.Label();
            this.WinsPanel = new System.Windows.Forms.Panel();
            this.FirstTimeGuessesCountLabel = new System.Windows.Forms.Label();
            this.FirstTimeGuessesLabel = new System.Windows.Forms.Label();
            this.winsCountLabel = new System.Windows.Forms.Label();
            this.winsLabel = new System.Windows.Forms.Label();
            this.PercentagePanel = new System.Windows.Forms.Panel();
            this.FirstTimeGuessesPercentageCountLabel = new System.Windows.Forms.Label();
            this.FirstTimeGuessesPercentageLabel = new System.Windows.Forms.Label();
            this.winsPercentageCountLabel = new System.Windows.Forms.Label();
            this.winsPercentageLabel = new System.Windows.Forms.Label();
            this.statsImagePanel = new System.Windows.Forms.Panel();
            this.statsScalePanel = new System.Windows.Forms.Panel();
            this.ResetStatsButton = new System.Windows.Forms.Button();
            this.separatorPanel = new System.Windows.Forms.Panel();
            this.wikiLink = new System.Windows.Forms.LinkLabel();
            this.modeSelectPanel.SuspendLayout();
            this.simulationPanel.SuspendLayout();
            this.SelectionPanel.SuspendLayout();
            this.StatusPanel.SuspendLayout();
            this.DoorsPanel.SuspendLayout();
            this.Door2PrizePanel.SuspendLayout();
            this.Door2Panel.SuspendLayout();
            this.Door1PrizePanel.SuspendLayout();
            this.Door1Panel.SuspendLayout();
            this.Door3PrizePanel.SuspendLayout();
            this.Door3Panel.SuspendLayout();
            this.AutoSelectPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.speedBar)).BeginInit();
            this.StatisticsPanel.SuspendLayout();
            this.InnerStatsPanel.SuspendLayout();
            this.LosesPanel.SuspendLayout();
            this.WinsPanel.SuspendLayout();
            this.PercentagePanel.SuspendLayout();
            this.statsImagePanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // modeSelectPanel
            // 
            this.modeSelectPanel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.modeSelectPanel.Controls.Add(this.wikiLink);
            this.modeSelectPanel.Controls.Add(this.descriptionText);
            this.modeSelectPanel.Dock = System.Windows.Forms.DockStyle.Top;
            this.modeSelectPanel.Location = new System.Drawing.Point(0, 0);
            this.modeSelectPanel.Name = "modeSelectPanel";
            this.modeSelectPanel.Size = new System.Drawing.Size(1000, 50);
            this.modeSelectPanel.TabIndex = 0;
            // 
            // descriptionText
            // 
            this.descriptionText.Dock = System.Windows.Forms.DockStyle.Fill;
            this.descriptionText.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.descriptionText.Location = new System.Drawing.Point(0, 0);
            this.descriptionText.Multiline = true;
            this.descriptionText.Name = "descriptionText";
            this.descriptionText.ReadOnly = true;
            this.descriptionText.Size = new System.Drawing.Size(998, 48);
            this.descriptionText.TabIndex = 0;
            this.descriptionText.Text = resources.GetString("descriptionText.Text");
            this.descriptionText.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // simulationPanel
            // 
            this.simulationPanel.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.simulationPanel.Controls.Add(this.SelectionPanel);
            this.simulationPanel.Controls.Add(this.StatisticsPanel);
            this.simulationPanel.Controls.Add(this.separatorPanel);
            this.simulationPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.simulationPanel.Location = new System.Drawing.Point(0, 50);
            this.simulationPanel.Name = "simulationPanel";
            this.simulationPanel.Size = new System.Drawing.Size(1000, 412);
            this.simulationPanel.TabIndex = 1;
            // 
            // SelectionPanel
            // 
            this.SelectionPanel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.SelectionPanel.Controls.Add(this.StatusPanel);
            this.SelectionPanel.Controls.Add(this.DoorsPanel);
            this.SelectionPanel.Controls.Add(this.AutoSelectPanel);
            this.SelectionPanel.Dock = System.Windows.Forms.DockStyle.Left;
            this.SelectionPanel.Location = new System.Drawing.Point(0, 12);
            this.SelectionPanel.Name = "SelectionPanel";
            this.SelectionPanel.Size = new System.Drawing.Size(500, 400);
            this.SelectionPanel.TabIndex = 2;
            this.SelectionPanel.Visible = false;
            // 
            // StatusPanel
            // 
            this.StatusPanel.BackColor = System.Drawing.SystemColors.ControlLight;
            this.StatusPanel.Controls.Add(this.switchSelectionRadio);
            this.StatusPanel.Controls.Add(this.persistantSelectionRadio);
            this.StatusPanel.Controls.Add(this.randomSelectionRadio);
            this.StatusPanel.Controls.Add(this.StatusTextbox);
            this.StatusPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.StatusPanel.Location = new System.Drawing.Point(0, 280);
            this.StatusPanel.Name = "StatusPanel";
            this.StatusPanel.Size = new System.Drawing.Size(498, 118);
            this.StatusPanel.TabIndex = 3;
            // 
            // switchSelectionRadio
            // 
            this.switchSelectionRadio.AutoSize = true;
            this.switchSelectionRadio.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.switchSelectionRadio.Location = new System.Drawing.Point(340, 79);
            this.switchSelectionRadio.Name = "switchSelectionRadio";
            this.switchSelectionRadio.Size = new System.Drawing.Size(127, 28);
            this.switchSelectionRadio.TabIndex = 3;
            this.switchSelectionRadio.Text = "Switch door";
            this.switchSelectionRadio.UseVisualStyleBackColor = true;
            // 
            // persistantSelectionRadio
            // 
            this.persistantSelectionRadio.AutoSize = true;
            this.persistantSelectionRadio.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.persistantSelectionRadio.Location = new System.Drawing.Point(215, 79);
            this.persistantSelectionRadio.Name = "persistantSelectionRadio";
            this.persistantSelectionRadio.Size = new System.Drawing.Size(117, 28);
            this.persistantSelectionRadio.TabIndex = 2;
            this.persistantSelectionRadio.Text = "Keep door";
            this.persistantSelectionRadio.UseVisualStyleBackColor = true;
            // 
            // randomSelectionRadio
            // 
            this.randomSelectionRadio.AutoSize = true;
            this.randomSelectionRadio.Checked = true;
            this.randomSelectionRadio.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.randomSelectionRadio.Location = new System.Drawing.Point(6, 79);
            this.randomSelectionRadio.Name = "randomSelectionRadio";
            this.randomSelectionRadio.Size = new System.Drawing.Size(178, 28);
            this.randomSelectionRadio.TabIndex = 1;
            this.randomSelectionRadio.TabStop = true;
            this.randomSelectionRadio.Text = "Ask me each time";
            this.randomSelectionRadio.UseVisualStyleBackColor = true;
            // 
            // StatusTextbox
            // 
            this.StatusTextbox.Dock = System.Windows.Forms.DockStyle.Top;
            this.StatusTextbox.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.StatusTextbox.Location = new System.Drawing.Point(0, 0);
            this.StatusTextbox.Multiline = true;
            this.StatusTextbox.Name = "StatusTextbox";
            this.StatusTextbox.ReadOnly = true;
            this.StatusTextbox.Size = new System.Drawing.Size(498, 73);
            this.StatusTextbox.TabIndex = 0;
            this.StatusTextbox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // DoorsPanel
            // 
            this.DoorsPanel.Controls.Add(this.Door2PrizePanel);
            this.DoorsPanel.Controls.Add(this.Door1PrizePanel);
            this.DoorsPanel.Controls.Add(this.Door3PrizePanel);
            this.DoorsPanel.Dock = System.Windows.Forms.DockStyle.Top;
            this.DoorsPanel.Location = new System.Drawing.Point(0, 30);
            this.DoorsPanel.Name = "DoorsPanel";
            this.DoorsPanel.Size = new System.Drawing.Size(498, 250);
            this.DoorsPanel.TabIndex = 2;
            // 
            // Door2PrizePanel
            // 
            this.Door2PrizePanel.BackColor = System.Drawing.Color.Transparent;
            this.Door2PrizePanel.Controls.Add(this.Door2Panel);
            this.Door2PrizePanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Door2PrizePanel.Location = new System.Drawing.Point(166, 0);
            this.Door2PrizePanel.Name = "Door2PrizePanel";
            this.Door2PrizePanel.Size = new System.Drawing.Size(166, 250);
            this.Door2PrizePanel.TabIndex = 1;
            // 
            // Door2Panel
            // 
            this.Door2Panel.Controls.Add(this.Door2SelectedPanel);
            this.Door2Panel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Door2Panel.Location = new System.Drawing.Point(0, 0);
            this.Door2Panel.Name = "Door2Panel";
            this.Door2Panel.Size = new System.Drawing.Size(166, 250);
            this.Door2Panel.TabIndex = 1;
            // 
            // Door2SelectedPanel
            // 
            this.Door2SelectedPanel.BackColor = System.Drawing.Color.Transparent;
            this.Door2SelectedPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Door2SelectedPanel.Location = new System.Drawing.Point(0, 0);
            this.Door2SelectedPanel.Name = "Door2SelectedPanel";
            this.Door2SelectedPanel.Size = new System.Drawing.Size(166, 250);
            this.Door2SelectedPanel.TabIndex = 1;
            this.Door2SelectedPanel.Click += new System.EventHandler(this.DoorPanel_Click);
            // 
            // Door1PrizePanel
            // 
            this.Door1PrizePanel.BackColor = System.Drawing.Color.Transparent;
            this.Door1PrizePanel.Controls.Add(this.Door1Panel);
            this.Door1PrizePanel.Dock = System.Windows.Forms.DockStyle.Left;
            this.Door1PrizePanel.Location = new System.Drawing.Point(0, 0);
            this.Door1PrizePanel.Name = "Door1PrizePanel";
            this.Door1PrizePanel.Size = new System.Drawing.Size(166, 250);
            this.Door1PrizePanel.TabIndex = 0;
            // 
            // Door1Panel
            // 
            this.Door1Panel.Controls.Add(this.Door1SelectedPanel);
            this.Door1Panel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Door1Panel.Location = new System.Drawing.Point(0, 0);
            this.Door1Panel.Name = "Door1Panel";
            this.Door1Panel.Size = new System.Drawing.Size(166, 250);
            this.Door1Panel.TabIndex = 0;
            // 
            // Door1SelectedPanel
            // 
            this.Door1SelectedPanel.BackColor = System.Drawing.Color.Transparent;
            this.Door1SelectedPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Door1SelectedPanel.Location = new System.Drawing.Point(0, 0);
            this.Door1SelectedPanel.Name = "Door1SelectedPanel";
            this.Door1SelectedPanel.Size = new System.Drawing.Size(166, 250);
            this.Door1SelectedPanel.TabIndex = 0;
            this.Door1SelectedPanel.Click += new System.EventHandler(this.DoorPanel_Click);
            // 
            // Door3PrizePanel
            // 
            this.Door3PrizePanel.BackColor = System.Drawing.Color.Transparent;
            this.Door3PrizePanel.Controls.Add(this.Door3Panel);
            this.Door3PrizePanel.Dock = System.Windows.Forms.DockStyle.Right;
            this.Door3PrizePanel.Location = new System.Drawing.Point(332, 0);
            this.Door3PrizePanel.Name = "Door3PrizePanel";
            this.Door3PrizePanel.Size = new System.Drawing.Size(166, 250);
            this.Door3PrizePanel.TabIndex = 2;
            // 
            // Door3Panel
            // 
            this.Door3Panel.Controls.Add(this.Door3SelectedPanel);
            this.Door3Panel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Door3Panel.Location = new System.Drawing.Point(0, 0);
            this.Door3Panel.Name = "Door3Panel";
            this.Door3Panel.Size = new System.Drawing.Size(166, 250);
            this.Door3Panel.TabIndex = 1;
            // 
            // Door3SelectedPanel
            // 
            this.Door3SelectedPanel.BackColor = System.Drawing.Color.Transparent;
            this.Door3SelectedPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Door3SelectedPanel.Location = new System.Drawing.Point(0, 0);
            this.Door3SelectedPanel.Name = "Door3SelectedPanel";
            this.Door3SelectedPanel.Size = new System.Drawing.Size(166, 250);
            this.Door3SelectedPanel.TabIndex = 1;
            this.Door3SelectedPanel.Click += new System.EventHandler(this.DoorPanel_Click);
            // 
            // AutoSelectPanel
            // 
            this.AutoSelectPanel.BackColor = System.Drawing.SystemColors.ControlLight;
            this.AutoSelectPanel.Controls.Add(this.cheatCheck);
            this.AutoSelectPanel.Controls.Add(this.speedLabel);
            this.AutoSelectPanel.Controls.Add(this.speedBar);
            this.AutoSelectPanel.Controls.Add(this.autoSelectCheckbox);
            this.AutoSelectPanel.Dock = System.Windows.Forms.DockStyle.Top;
            this.AutoSelectPanel.Location = new System.Drawing.Point(0, 0);
            this.AutoSelectPanel.Name = "AutoSelectPanel";
            this.AutoSelectPanel.Size = new System.Drawing.Size(498, 30);
            this.AutoSelectPanel.TabIndex = 1;
            // 
            // cheatCheck
            // 
            this.cheatCheck.AutoSize = true;
            this.cheatCheck.Location = new System.Drawing.Point(204, 5);
            this.cheatCheck.Name = "cheatCheck";
            this.cheatCheck.Size = new System.Drawing.Size(83, 17);
            this.cheatCheck.TabIndex = 5;
            this.cheatCheck.Text = "Cheat mode";
            this.cheatCheck.UseVisualStyleBackColor = true;
            this.cheatCheck.CheckedChanged += new System.EventHandler(this.cheatCheck_CheckedChanged);
            // 
            // speedLabel
            // 
            this.speedLabel.AutoSize = true;
            this.speedLabel.Location = new System.Drawing.Point(308, 7);
            this.speedLabel.Name = "speedLabel";
            this.speedLabel.Size = new System.Drawing.Size(38, 13);
            this.speedLabel.TabIndex = 3;
            this.speedLabel.Text = "Speed";
            // 
            // speedBar
            // 
            this.speedBar.Location = new System.Drawing.Point(338, 2);
            this.speedBar.Maximum = 60;
            this.speedBar.Minimum = 1;
            this.speedBar.Name = "speedBar";
            this.speedBar.Size = new System.Drawing.Size(160, 45);
            this.speedBar.TabIndex = 4;
            this.speedBar.Value = 3;
            // 
            // autoSelectCheckbox
            // 
            this.autoSelectCheckbox.AutoSize = true;
            this.autoSelectCheckbox.Location = new System.Drawing.Point(12, 6);
            this.autoSelectCheckbox.Name = "autoSelectCheckbox";
            this.autoSelectCheckbox.Size = new System.Drawing.Size(183, 17);
            this.autoSelectCheckbox.TabIndex = 0;
            this.autoSelectCheckbox.Text = "Automate (Randomly pick a door)";
            this.autoSelectCheckbox.UseVisualStyleBackColor = true;
            // 
            // StatisticsPanel
            // 
            this.StatisticsPanel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.StatisticsPanel.Controls.Add(this.InnerStatsPanel);
            this.StatisticsPanel.Dock = System.Windows.Forms.DockStyle.Right;
            this.StatisticsPanel.Location = new System.Drawing.Point(500, 12);
            this.StatisticsPanel.Name = "StatisticsPanel";
            this.StatisticsPanel.Size = new System.Drawing.Size(500, 400);
            this.StatisticsPanel.TabIndex = 1;
            this.StatisticsPanel.Visible = false;
            // 
            // InnerStatsPanel
            // 
            this.InnerStatsPanel.BackColor = System.Drawing.SystemColors.ControlLight;
            this.InnerStatsPanel.Controls.Add(this.LosesPanel);
            this.InnerStatsPanel.Controls.Add(this.WinsPanel);
            this.InnerStatsPanel.Controls.Add(this.PercentagePanel);
            this.InnerStatsPanel.Controls.Add(this.statsImagePanel);
            this.InnerStatsPanel.Controls.Add(this.ResetStatsButton);
            this.InnerStatsPanel.Location = new System.Drawing.Point(10, 10);
            this.InnerStatsPanel.Margin = new System.Windows.Forms.Padding(0);
            this.InnerStatsPanel.Name = "InnerStatsPanel";
            this.InnerStatsPanel.Size = new System.Drawing.Size(480, 380);
            this.InnerStatsPanel.TabIndex = 0;
            // 
            // LosesPanel
            // 
            this.LosesPanel.Controls.Add(this.FirstTimeMissesCountLabel);
            this.LosesPanel.Controls.Add(this.FirstTimeMissesLabel);
            this.LosesPanel.Controls.Add(this.losesCountLabel);
            this.LosesPanel.Controls.Add(this.losesLabel);
            this.LosesPanel.Dock = System.Windows.Forms.DockStyle.Left;
            this.LosesPanel.Location = new System.Drawing.Point(160, 0);
            this.LosesPanel.Name = "LosesPanel";
            this.LosesPanel.Size = new System.Drawing.Size(160, 104);
            this.LosesPanel.TabIndex = 2;
            // 
            // FirstTimeMissesCountLabel
            // 
            this.FirstTimeMissesCountLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.FirstTimeMissesCountLabel.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.FirstTimeMissesCountLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.FirstTimeMissesCountLabel.ForeColor = System.Drawing.Color.Coral;
            this.FirstTimeMissesCountLabel.Location = new System.Drawing.Point(119, 7);
            this.FirstTimeMissesCountLabel.Name = "FirstTimeMissesCountLabel";
            this.FirstTimeMissesCountLabel.Size = new System.Drawing.Size(32, 17);
            this.FirstTimeMissesCountLabel.TabIndex = 4;
            this.FirstTimeMissesCountLabel.Text = "0";
            this.FirstTimeMissesCountLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.FirstTimeMissesCountLabel.UseCompatibleTextRendering = true;
            // 
            // FirstTimeMissesLabel
            // 
            this.FirstTimeMissesLabel.AutoSize = true;
            this.FirstTimeMissesLabel.Location = new System.Drawing.Point(6, 7);
            this.FirstTimeMissesLabel.Name = "FirstTimeMissesLabel";
            this.FirstTimeMissesLabel.Size = new System.Drawing.Size(86, 13);
            this.FirstTimeMissesLabel.TabIndex = 3;
            this.FirstTimeMissesLabel.Text = "First pick misses:";
            // 
            // losesCountLabel
            // 
            this.losesCountLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.losesCountLabel.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.losesCountLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 28F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.losesCountLabel.ForeColor = System.Drawing.Color.Coral;
            this.losesCountLabel.Location = new System.Drawing.Point(6, 48);
            this.losesCountLabel.Name = "losesCountLabel";
            this.losesCountLabel.Size = new System.Drawing.Size(149, 50);
            this.losesCountLabel.TabIndex = 2;
            this.losesCountLabel.Text = "0";
            this.losesCountLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.losesCountLabel.UseCompatibleTextRendering = true;
            // 
            // losesLabel
            // 
            this.losesLabel.AutoSize = true;
            this.losesLabel.Location = new System.Drawing.Point(6, 31);
            this.losesLabel.Name = "losesLabel";
            this.losesLabel.Size = new System.Drawing.Size(31, 13);
            this.losesLabel.TabIndex = 1;
            this.losesLabel.Text = "Fails:";
            // 
            // WinsPanel
            // 
            this.WinsPanel.Controls.Add(this.FirstTimeGuessesCountLabel);
            this.WinsPanel.Controls.Add(this.FirstTimeGuessesLabel);
            this.WinsPanel.Controls.Add(this.winsCountLabel);
            this.WinsPanel.Controls.Add(this.winsLabel);
            this.WinsPanel.Dock = System.Windows.Forms.DockStyle.Left;
            this.WinsPanel.Location = new System.Drawing.Point(0, 0);
            this.WinsPanel.Name = "WinsPanel";
            this.WinsPanel.Size = new System.Drawing.Size(160, 104);
            this.WinsPanel.TabIndex = 1;
            // 
            // FirstTimeGuessesCountLabel
            // 
            this.FirstTimeGuessesCountLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.FirstTimeGuessesCountLabel.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.FirstTimeGuessesCountLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.FirstTimeGuessesCountLabel.ForeColor = System.Drawing.Color.GreenYellow;
            this.FirstTimeGuessesCountLabel.Location = new System.Drawing.Point(119, 7);
            this.FirstTimeGuessesCountLabel.Name = "FirstTimeGuessesCountLabel";
            this.FirstTimeGuessesCountLabel.Size = new System.Drawing.Size(32, 17);
            this.FirstTimeGuessesCountLabel.TabIndex = 3;
            this.FirstTimeGuessesCountLabel.Text = "0";
            this.FirstTimeGuessesCountLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.FirstTimeGuessesCountLabel.UseCompatibleTextRendering = true;
            // 
            // FirstTimeGuessesLabel
            // 
            this.FirstTimeGuessesLabel.AutoSize = true;
            this.FirstTimeGuessesLabel.Location = new System.Drawing.Point(3, 7);
            this.FirstTimeGuessesLabel.Name = "FirstTimeGuessesLabel";
            this.FirstTimeGuessesLabel.Size = new System.Drawing.Size(94, 13);
            this.FirstTimeGuessesLabel.TabIndex = 2;
            this.FirstTimeGuessesLabel.Text = "First pick guesses:";
            // 
            // winsCountLabel
            // 
            this.winsCountLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.winsCountLabel.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.winsCountLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 28F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.winsCountLabel.ForeColor = System.Drawing.Color.GreenYellow;
            this.winsCountLabel.Location = new System.Drawing.Point(6, 48);
            this.winsCountLabel.Name = "winsCountLabel";
            this.winsCountLabel.Size = new System.Drawing.Size(149, 50);
            this.winsCountLabel.TabIndex = 1;
            this.winsCountLabel.Text = "0";
            this.winsCountLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.winsCountLabel.UseCompatibleTextRendering = true;
            // 
            // winsLabel
            // 
            this.winsLabel.AutoSize = true;
            this.winsLabel.Location = new System.Drawing.Point(3, 31);
            this.winsLabel.Name = "winsLabel";
            this.winsLabel.Size = new System.Drawing.Size(34, 13);
            this.winsLabel.TabIndex = 0;
            this.winsLabel.Text = "Wins:";
            // 
            // PercentagePanel
            // 
            this.PercentagePanel.Controls.Add(this.FirstTimeGuessesPercentageCountLabel);
            this.PercentagePanel.Controls.Add(this.FirstTimeGuessesPercentageLabel);
            this.PercentagePanel.Controls.Add(this.winsPercentageCountLabel);
            this.PercentagePanel.Controls.Add(this.winsPercentageLabel);
            this.PercentagePanel.Dock = System.Windows.Forms.DockStyle.Right;
            this.PercentagePanel.Location = new System.Drawing.Point(320, 0);
            this.PercentagePanel.Name = "PercentagePanel";
            this.PercentagePanel.Size = new System.Drawing.Size(160, 104);
            this.PercentagePanel.TabIndex = 3;
            // 
            // FirstTimeGuessesPercentageCountLabel
            // 
            this.FirstTimeGuessesPercentageCountLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.FirstTimeGuessesPercentageCountLabel.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.FirstTimeGuessesPercentageCountLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.FirstTimeGuessesPercentageCountLabel.Location = new System.Drawing.Point(102, 7);
            this.FirstTimeGuessesPercentageCountLabel.Name = "FirstTimeGuessesPercentageCountLabel";
            this.FirstTimeGuessesPercentageCountLabel.Size = new System.Drawing.Size(52, 17);
            this.FirstTimeGuessesPercentageCountLabel.TabIndex = 3;
            this.FirstTimeGuessesPercentageCountLabel.Text = "0";
            this.FirstTimeGuessesPercentageCountLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.FirstTimeGuessesPercentageCountLabel.UseCompatibleTextRendering = true;
            // 
            // FirstTimeGuessesPercentageLabel
            // 
            this.FirstTimeGuessesPercentageLabel.AutoSize = true;
            this.FirstTimeGuessesPercentageLabel.Location = new System.Drawing.Point(3, 7);
            this.FirstTimeGuessesPercentageLabel.Name = "FirstTimeGuessesPercentageLabel";
            this.FirstTimeGuessesPercentageLabel.Size = new System.Drawing.Size(94, 13);
            this.FirstTimeGuessesPercentageLabel.TabIndex = 2;
            this.FirstTimeGuessesPercentageLabel.Text = "First pick guesses:";
            // 
            // winsPercentageCountLabel
            // 
            this.winsPercentageCountLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.winsPercentageCountLabel.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.winsPercentageCountLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.winsPercentageCountLabel.Location = new System.Drawing.Point(6, 48);
            this.winsPercentageCountLabel.Name = "winsPercentageCountLabel";
            this.winsPercentageCountLabel.Size = new System.Drawing.Size(149, 50);
            this.winsPercentageCountLabel.TabIndex = 1;
            this.winsPercentageCountLabel.Text = "0";
            this.winsPercentageCountLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.winsPercentageCountLabel.UseCompatibleTextRendering = true;
            // 
            // winsPercentageLabel
            // 
            this.winsPercentageLabel.AutoSize = true;
            this.winsPercentageLabel.Location = new System.Drawing.Point(3, 31);
            this.winsPercentageLabel.Name = "winsPercentageLabel";
            this.winsPercentageLabel.Size = new System.Drawing.Size(34, 13);
            this.winsPercentageLabel.TabIndex = 0;
            this.winsPercentageLabel.Text = "Wins:";
            // 
            // statsImagePanel
            // 
            this.statsImagePanel.BackColor = System.Drawing.Color.White;
            this.statsImagePanel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.statsImagePanel.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.statsImagePanel.Controls.Add(this.statsScalePanel);
            this.statsImagePanel.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.statsImagePanel.Location = new System.Drawing.Point(0, 104);
            this.statsImagePanel.Name = "statsImagePanel";
            this.statsImagePanel.Size = new System.Drawing.Size(480, 250);
            this.statsImagePanel.TabIndex = 0;
            // 
            // statsScalePanel
            // 
            this.statsScalePanel.BackColor = System.Drawing.Color.Transparent;
            this.statsScalePanel.BackgroundImage = global::ofzza.MontyHallProblem.Properties.Resources.scale;
            this.statsScalePanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.statsScalePanel.Location = new System.Drawing.Point(0, 0);
            this.statsScalePanel.Name = "statsScalePanel";
            this.statsScalePanel.Size = new System.Drawing.Size(476, 246);
            this.statsScalePanel.TabIndex = 0;
            // 
            // ResetStatsButton
            // 
            this.ResetStatsButton.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.ResetStatsButton.Location = new System.Drawing.Point(0, 354);
            this.ResetStatsButton.Name = "ResetStatsButton";
            this.ResetStatsButton.Size = new System.Drawing.Size(480, 26);
            this.ResetStatsButton.TabIndex = 0;
            this.ResetStatsButton.Text = "Reset!";
            this.ResetStatsButton.UseVisualStyleBackColor = true;
            this.ResetStatsButton.Click += new System.EventHandler(this.ResetStatsButton_Click);
            // 
            // separatorPanel
            // 
            this.separatorPanel.BackColor = System.Drawing.SystemColors.ButtonShadow;
            this.separatorPanel.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.separatorPanel.Dock = System.Windows.Forms.DockStyle.Top;
            this.separatorPanel.Location = new System.Drawing.Point(0, 0);
            this.separatorPanel.Name = "separatorPanel";
            this.separatorPanel.Size = new System.Drawing.Size(1000, 12);
            this.separatorPanel.TabIndex = 0;
            // 
            // wikiLink
            // 
            this.wikiLink.AutoSize = true;
            this.wikiLink.BackColor = System.Drawing.SystemColors.Control;
            this.wikiLink.Cursor = System.Windows.Forms.Cursors.Hand;
            this.wikiLink.Location = new System.Drawing.Point(874, 33);
            this.wikiLink.Name = "wikiLink";
            this.wikiLink.Size = new System.Drawing.Size(121, 13);
            this.wikiLink.TabIndex = 1;
            this.wikiLink.TabStop = true;
            this.wikiLink.Text = "Read more at Wikipedia";
            this.wikiLink.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.wikiLink_LinkClicked);
            // 
            // mainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1000, 462);
            this.Controls.Add(this.simulationPanel);
            this.Controls.Add(this.modeSelectPanel);
            this.MaximumSize = new System.Drawing.Size(1016, 500);
            this.MinimumSize = new System.Drawing.Size(1016, 500);
            this.Name = "mainForm";
            this.Text = "Monty Hall problem";
            this.Load += new System.EventHandler(this.mainForm_Load);
            this.modeSelectPanel.ResumeLayout(false);
            this.modeSelectPanel.PerformLayout();
            this.simulationPanel.ResumeLayout(false);
            this.SelectionPanel.ResumeLayout(false);
            this.StatusPanel.ResumeLayout(false);
            this.StatusPanel.PerformLayout();
            this.DoorsPanel.ResumeLayout(false);
            this.Door2PrizePanel.ResumeLayout(false);
            this.Door2Panel.ResumeLayout(false);
            this.Door1PrizePanel.ResumeLayout(false);
            this.Door1Panel.ResumeLayout(false);
            this.Door3PrizePanel.ResumeLayout(false);
            this.Door3Panel.ResumeLayout(false);
            this.AutoSelectPanel.ResumeLayout(false);
            this.AutoSelectPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.speedBar)).EndInit();
            this.StatisticsPanel.ResumeLayout(false);
            this.InnerStatsPanel.ResumeLayout(false);
            this.LosesPanel.ResumeLayout(false);
            this.LosesPanel.PerformLayout();
            this.WinsPanel.ResumeLayout(false);
            this.WinsPanel.PerformLayout();
            this.PercentagePanel.ResumeLayout(false);
            this.PercentagePanel.PerformLayout();
            this.statsImagePanel.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel modeSelectPanel;
        private System.Windows.Forms.Panel simulationPanel;
        private System.Windows.Forms.Panel separatorPanel;
        private System.Windows.Forms.Panel SelectionPanel;
        private System.Windows.Forms.Panel StatisticsPanel;
        private System.Windows.Forms.CheckBox autoSelectCheckbox;
        private System.Windows.Forms.Panel StatusPanel;
        private System.Windows.Forms.Panel DoorsPanel;
        private System.Windows.Forms.Panel AutoSelectPanel;
        private System.Windows.Forms.Panel Door2PrizePanel;
        private System.Windows.Forms.Panel Door1PrizePanel;
        private System.Windows.Forms.Panel Door3PrizePanel;
        private System.Windows.Forms.Panel InnerStatsPanel;
        private System.Windows.Forms.TextBox StatusTextbox;
        private System.Windows.Forms.Panel Door2Panel;
        private System.Windows.Forms.Panel Door1Panel;
        private System.Windows.Forms.Panel Door3Panel;
        private System.Windows.Forms.Panel Door2SelectedPanel;
        private System.Windows.Forms.Panel Door1SelectedPanel;
        private System.Windows.Forms.Panel Door3SelectedPanel;
        private System.Windows.Forms.Panel LosesPanel;
        private System.Windows.Forms.Panel WinsPanel;
        private System.Windows.Forms.Panel statsImagePanel;
        private System.Windows.Forms.Label losesCountLabel;
        private System.Windows.Forms.Label losesLabel;
        private System.Windows.Forms.Label winsCountLabel;
        private System.Windows.Forms.Label winsLabel;
        private System.Windows.Forms.Label speedLabel;
        private System.Windows.Forms.Label FirstTimeMissesLabel;
        private System.Windows.Forms.Label FirstTimeGuessesLabel;
        private System.Windows.Forms.Label FirstTimeMissesCountLabel;
        private System.Windows.Forms.Label FirstTimeGuessesCountLabel;
        private System.Windows.Forms.Panel statsScalePanel;
        private System.Windows.Forms.TextBox descriptionText;
        private System.Windows.Forms.Button ResetStatsButton;
        private System.Windows.Forms.TrackBar speedBar;
        private System.Windows.Forms.Panel PercentagePanel;
        private System.Windows.Forms.Label FirstTimeGuessesPercentageCountLabel;
        private System.Windows.Forms.Label FirstTimeGuessesPercentageLabel;
        private System.Windows.Forms.Label winsPercentageCountLabel;
        private System.Windows.Forms.Label winsPercentageLabel;
        private System.Windows.Forms.RadioButton switchSelectionRadio;
        private System.Windows.Forms.RadioButton persistantSelectionRadio;
        private System.Windows.Forms.RadioButton randomSelectionRadio;
        private System.Windows.Forms.CheckBox cheatCheck;
        private System.Windows.Forms.LinkLabel wikiLink;
    }
}

