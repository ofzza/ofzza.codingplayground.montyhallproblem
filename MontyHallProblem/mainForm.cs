﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ofzza.MontyHallProblem {

    public partial class mainForm : Form, INotifyPropertyChanged {

        // =============================================================================================================================
        // Constructors & initialization
        // =============================================================================================================================
        #region Constructors & initialization

        /// <summary>
        /// Constructor
        /// </summary>
        public mainForm() {
            // Initialize components
            InitializeComponent();

            // Initialize bindings
            this.Door1PrizePanel.DataBindings.Add("BackgroundImage", this, "Door1PrizeImage", false, DataSourceUpdateMode.OnPropertyChanged);
            this.Door1Panel.DataBindings.Add("BackgroundImage", this, "Door1Image", false, DataSourceUpdateMode.OnPropertyChanged);
            this.Door1SelectedPanel.DataBindings.Add("BackgroundImage", this, "Door1SelectedImage", false, DataSourceUpdateMode.OnPropertyChanged);
            
            this.Door2PrizePanel.DataBindings.Add("BackgroundImage", this, "Door2PrizeImage", false, DataSourceUpdateMode.OnPropertyChanged);
            this.Door2Panel.DataBindings.Add("BackgroundImage", this, "Door2Image", false, DataSourceUpdateMode.OnPropertyChanged);
            this.Door2SelectedPanel.DataBindings.Add("BackgroundImage", this, "Door2SelectedImage", false, DataSourceUpdateMode.OnPropertyChanged);
            
            this.Door3PrizePanel.DataBindings.Add("BackgroundImage", this, "Door3PrizeImage", false, DataSourceUpdateMode.OnPropertyChanged);
            this.Door3Panel.DataBindings.Add("BackgroundImage", this, "Door3Image", false, DataSourceUpdateMode.OnPropertyChanged);
            this.Door3SelectedPanel.DataBindings.Add("BackgroundImage", this, "Door3SelectedImage", false, DataSourceUpdateMode.OnPropertyChanged);

            this.FirstTimeGuessesCountLabel.DataBindings.Add("Text", this, "FirstTimeGuesses", false, DataSourceUpdateMode.OnPropertyChanged);
            this.FirstTimeMissesCountLabel.DataBindings.Add("Text", this, "FirstTimeMisses", false, DataSourceUpdateMode.OnPropertyChanged);
            this.FirstTimeGuessesPercentageCountLabel.DataBindings.Add("Text", this, "FirstTimeGuessesPercentage", false, DataSourceUpdateMode.OnPropertyChanged);
            this.winsCountLabel.DataBindings.Add("Text", this, "Wins", false, DataSourceUpdateMode.OnPropertyChanged);
            this.losesCountLabel.DataBindings.Add("Text", this, "Loses", false, DataSourceUpdateMode.OnPropertyChanged);
            this.winsPercentageCountLabel.DataBindings.Add("Text", this, "WinsPercentage", false, DataSourceUpdateMode.OnPropertyChanged);
            this.statsImagePanel.DataBindings.Add("BackgroundImage", this, "StatsImage", false, DataSourceUpdateMode.OnPropertyChanged);
        }

        /// <summary>
        /// On loaded
        /// </summary>
        private void mainForm_Load(object sender, EventArgs e) {
            this.ResetSimulation();
            this.StartSimulation();
        }

        #endregion

        // =============================================================================================================================
        // INotifyPropertyChanged implementation
        // =============================================================================================================================
        #region INotifyPropertyChanged implementation

        /// <summary>
        /// Implements INotifyPropertyChanged interface
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;
        /// <summary>
        /// Implements INotifyPropertyChanged interface
        /// </summary>
        protected void OnPropertyChanged(PropertyChangedEventArgs e) {
            var evt = this.PropertyChanged;

            if (evt != null)
                evt(this, e);
        }

        #endregion

        // =============================================================================================================================
        // Properties
        // =============================================================================================================================
        #region Properties

        // General propertries
        // ------------------------------------------------------------------------------------
        #region General properties

        /// <summary>
        /// Holds alllow select status
        /// </summary>
        protected bool _allowSelect = false;

        /// <summary>
        /// Holds local randomizer object
        /// </summary>
        protected Random rand = new Random();

        /// <summary>
        /// Holds persist / Change selection status
        /// </summary>
        protected bool PersistSelection {
            get {
                if (this.switchSelectionRadio.Checked) return false;
                if (this.persistantSelectionRadio.Checked) return true;
                if (this.randomSelectionRadio.Checked) {
                    if (this.autoSelectCheckbox.Checked) {
                        return (this.rand.Next(1, 3) == 1);
                    } else {
                        return (MessageBox.Show("Do you wish to switch your choice?", "Switch?", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No);
                    }
                } else {
                    return false;
                }
            }
        }

        #endregion

        // Statistics
        // ------------------------------------------------------------------------------------
        #region Statistics

        /// <summary>
        /// Holds total first time guesses count
        /// </summary>
        protected int _ftGuesses = 0;
        /// <summary>
        /// Gets total first time guesses count
        /// </summary>
        public int FirstTimeGuesses { get { return this._ftGuesses; } }
        /// <summary>
        /// Holds total first time misses count
        /// </summary>
        protected int _ftMisses = 0;
        /// <summary>
        /// Gets total first time misses count
        /// </summary>
        public int FirstTimeMisses { get { return this._ftMisses; } }
        /// <summary>
        /// Gets first time wins percentage
        /// </summary>
        public string FirstTimeGuessesPercentage { get { return string.Format("{0:N2}%", 100 * ((this._ftGuesses + this._ftMisses) > 0 ? (double)this._ftGuesses / ((double)this._ftGuesses + (double)this._ftMisses) : 0)); } }

        /// <summary>
        /// Holds total wins count
        /// </summary>
        protected int _wins = 0;
        /// <summary>
        /// Gets total wins count
        /// </summary>
        public int Wins { get { return this._wins; } }
        /// <summary>
        /// Holds total loses count
        /// </summary>
        protected int _loses = 0;
        /// <summary>
        /// Gets total loses count
        /// </summary>
        public int Loses { get { return this._loses; } }
        /// <summary>
        /// Gets first time wins percentage
        /// </summary>
        public string WinsPercentage { get { return string.Format("{0:N2}%", 100 * ((this._wins + this._loses) > 0 ? (double)this._wins / ((double)this._wins + (double)this._loses) : 0)); } }
        /// <summary>
        /// Holds wins / loses stats
        /// </summary>
        protected List<double> _stats = new List<double>();
        /// <summary>
        /// Holds stats rendered graph
        /// </summary>
        protected Image _statsImage = new Bitmap(1, 300);
        /// <summary>
        /// Gets stats rendered graph
        /// </summary>
        public Image StatsImage {
            get { return this._statsImage; }
            set {
                // Set value
                this._statsImage = value;
                // Notify property changed
                this.OnPropertyChanged(new PropertyChangedEventArgs("StatsImage"));
            }
        }

        #endregion

        // Simulation current status
        // ------------------------------------------------------------------------------------
        #region Simulation current status

        /// <summary>
        /// Holds winning dor status
        /// </summary>
        protected int _winningDoor = 0;
        /// <summary>
        /// Sets winning dor status
        /// </summary>
        protected int WinningDoor {
            set {
                // Set value
                this._winningDoor = value;
                // Notify property changed
                this.OnPropertyChanged(new PropertyChangedEventArgs("WinningDoor1"));
                this.OnPropertyChanged(new PropertyChangedEventArgs("Door1PrizeImage"));
                this.OnPropertyChanged(new PropertyChangedEventArgs("WinningDoor2"));
                this.OnPropertyChanged(new PropertyChangedEventArgs("Door2PrizeImage"));
                this.OnPropertyChanged(new PropertyChangedEventArgs("WinningDoor3"));
                this.OnPropertyChanged(new PropertyChangedEventArgs("Door3PrizeImage"));
            }
        }

        /// <summary>
        /// Gets 1. door's winning status
        /// </summary>
        public bool WinningDoor1 { get { return (this._winningDoor == 1); } }
        /// <summary>
        /// gets 1. door's winning status image
        /// </summary>
        public Image Door1PrizeImage { get { return (this.WinningDoor1 ? (this.cheatCheck.Checked ? Properties.Resources.prize_cheat : Properties.Resources.prize) : (this.cheatCheck.Checked ? Properties.Resources.noprize_cheat : Properties.Resources.noprize)); } }
        /// <summary>
        /// Gets 2. door's winning status
        /// </summary>
        public bool WinningDoor2 { get { return (this._winningDoor == 2); } }
        /// <summary>
        /// gets 1. door's winning status image
        /// </summary>
        public Image Door2PrizeImage { get { return (this.WinningDoor2 ? (this.cheatCheck.Checked ? Properties.Resources.prize_cheat : Properties.Resources.prize) : (this.cheatCheck.Checked ? Properties.Resources.noprize_cheat : Properties.Resources.noprize)); } }
        /// <summary>
        /// Gets 3. door's winning status
        /// </summary>
        public bool WinningDoor3 { get { return (this._winningDoor == 3); } }
        /// <summary>
        /// gets 1. door's winning status image
        /// </summary>
        public Image Door3PrizeImage { get { return (this.WinningDoor3 ? (this.cheatCheck.Checked ? Properties.Resources.prize_cheat : Properties.Resources.prize) : (this.cheatCheck.Checked ? Properties.Resources.noprize_cheat : Properties.Resources.noprize)); } }

        /// <summary>
        /// Holds 1. door's open status
        /// </summary>
        protected bool _isDoor1Open = false;
        /// <summary>
        /// Gets / Sets 1. door's open status
        /// </summary>
        public bool IsDoor1Open {
            get { return this._isDoor1Open; }
            set {
                // Set value
                this._isDoor1Open = value;
                // Notify property changed
                this.OnPropertyChanged(new PropertyChangedEventArgs("IsDoor1Open"));
                this.OnPropertyChanged(new PropertyChangedEventArgs("Door1Image"));
            }
        }
        /// <summary>
        /// Gets 1. door's open status image
        /// </summary>
        public Image Door1Image { get { return (this.IsDoor1Open ? (this.autoSelectCheckbox.Checked ? Properties.Resources.open_transparent : Properties.Resources.open) : (this.autoSelectCheckbox.Checked ? Properties.Resources.closed_transparent : Properties.Resources.closed)); } }
        /// <summary>
        /// Holds 2. door's open status
        /// </summary>
        protected bool _isDoor2Open = false;
        /// <summary>
        /// Gets / Sets 2. door's open status
        /// </summary>
        public bool IsDoor2Open {
            get { return this._isDoor2Open; }
            set {
                // Set value
                this._isDoor2Open = value;
                // Notify property changed
                this.OnPropertyChanged(new PropertyChangedEventArgs("IsDoor2Open"));
                this.OnPropertyChanged(new PropertyChangedEventArgs("Door2Image"));
            }
        }
        /// <summary>
        /// Gets 2. door's open status image
        /// </summary>
        public Image Door2Image { get { return (this.IsDoor2Open ? (this.autoSelectCheckbox.Checked ? Properties.Resources.open_transparent : Properties.Resources.open) : (this.autoSelectCheckbox.Checked ? Properties.Resources.closed_transparent : Properties.Resources.closed)); } }
        /// <summary>
        /// Holds 3. door's open status
        /// </summary>
        protected bool _isDoor3Open = false;
        /// <summary>
        /// Gets / Sets 3. door's open status
        /// </summary>
        public bool IsDoor3Open {
            get { return this._isDoor3Open; }
            set {
                // Set value
                this._isDoor3Open = value;
                // Notify property changed
                this.OnPropertyChanged(new PropertyChangedEventArgs("IsDoor3Open"));
                this.OnPropertyChanged(new PropertyChangedEventArgs("Door3Image"));
            }
        }
        /// <summary>
        /// Gets 3. door's open status image
        /// </summary>
        public Image Door3Image { get { return (this.IsDoor3Open ? (this.autoSelectCheckbox.Checked ? Properties.Resources.open_transparent : Properties.Resources.open) : (this.autoSelectCheckbox.Checked ? Properties.Resources.closed_transparent : Properties.Resources.closed)); } }

        /// <summary>
        /// Holds selected door
        /// </summary>
        protected int _selectedDoor = 0;
        /// <summary>
        /// Sets selected door
        /// </summary>
        public int SelectedDoor {
            set {
                // Set value
                this._selectedDoor = value;
                // Notify property changed
                this.OnPropertyChanged(new PropertyChangedEventArgs("IsDoor1Selected"));
                this.OnPropertyChanged(new PropertyChangedEventArgs("Door1SelectedImage"));
                this.OnPropertyChanged(new PropertyChangedEventArgs("IsDoor2Selected"));
                this.OnPropertyChanged(new PropertyChangedEventArgs("Door2SelectedImage"));
                this.OnPropertyChanged(new PropertyChangedEventArgs("IsDoor3Selected"));
                this.OnPropertyChanged(new PropertyChangedEventArgs("Door3SelectedImage"));
            }
        }

        /// <summary>
        /// Gets 1. door's selected status
        /// </summary>
        public bool IsDoor1Selected { get { return this._selectedDoor == 1; } }
        /// <summary>
        /// Gets 1. door's slected image
        /// </summary>
        public Image Door1SelectedImage { get { return (this.IsDoor1Selected ? Properties.Resources.selected : Properties.Resources._null); } }
        /// <summary>
        /// Gets 2. door's selected status
        /// </summary>
        public bool IsDoor2Selected { get { return this._selectedDoor == 2; } }
        /// <summary>
        /// Gets 2. door's slected image
        /// </summary>
        public Image Door2SelectedImage { get { return (this.IsDoor2Selected ? Properties.Resources.selected : Properties.Resources._null); } }
        /// <summary>
        /// Gets 3. door's selected status
        /// </summary>
        public bool IsDoor3Selected { get { return this._selectedDoor == 3; } }
        /// <summary>
        /// Gets 3. door's slected image
        /// </summary>
        public Image Door3SelectedImage { get { return (this.IsDoor3Selected ? Properties.Resources.selected : Properties.Resources._null); } }

        #endregion

        #endregion

        // =============================================================================================================================
        // Application running functionality
        // =============================================================================================================================
        #region Application running functionality

        /// <summary>
        /// Resets simulation
        /// </summary>
        protected void ResetSimulation() {
            // Enable simulation panel
            this.simulationPanel.BackColor = Color.White;
            this.StatisticsPanel.Visible = true;
            this.SelectionPanel.Visible = true;
        }
        /// <summary>
        /// Starts simulation
        /// </summary>
        protected void StartSimulation() {
            // Setup choice
            this.SetupChoice();
        }

        /// <summary>
        /// Sets up choice (Step 1)
        /// </summary>
        protected void SetupChoice() {
            // Prompt status
            this.WinningDoor = this.rand.Next(1, 4);
            this.SelectedDoor = 0;
            this.StatusTextbox.Text = "Choose a door?";
            // Set selection mode
            this.IsDoor1Open = false;
            this.Door1Panel.Cursor = Cursors.Hand;
            this.IsDoor2Open = false;
            this.Door2Panel.Cursor = Cursors.Hand;
            this.IsDoor3Open = false;
            this.Door3Panel.Cursor = Cursors.Hand;
            // Check if auto select door
            if (this.autoSelectCheckbox.Checked) {
                // Pause
                double timeout = 2123 / this.speedBar.Value;
                System.Timers.Timer timer = new System.Timers.Timer(timeout);
                timer.Elapsed += (sender, args) => {
                                    // Stop timer
                                    timer.Stop();
                                     // Auto choose door
                                     this.Invoke(new Action(this.ChooseDoor));
                                 };
                timer.Start();
            } else {
                // Allow select
                this._allowSelect = true;
            }
        }

        /// <summary>
        /// Chooses door (Step 2.2)
        /// </summary>
        protected void ChooseDoor() {
            this.ChooseDoor(0);
        }
        /// <summary>
        /// Chooses door (Step 2.2)
        /// </summary>
        /// <param name="door"></param>
        protected void ChooseDoor(int door) {
            // Disallow select
            this._allowSelect = false;
            // Check if previously selected
            if (this._selectedDoor == 0) {
                // Disable selection mode
                this.Door1Panel.Cursor = Cursors.Default;
                this.Door2Panel.Cursor = Cursors.Default;
                this.Door3Panel.Cursor = Cursors.Default;
                // Do select
                if (door == 0) door = this.rand.Next(1, 4);
                this.SelectedDoor = door;
                // Check if first time guess / miss
                if (this._selectedDoor == this._winningDoor) {
                    this._ftGuesses++;
                    this.OnPropertyChanged(new PropertyChangedEventArgs("FirstTimeGuesses"));
                } else {
                    this._ftMisses++;
                    this.OnPropertyChanged(new PropertyChangedEventArgs("FirstTimeMisses"));
                }
                this.OnPropertyChanged(new PropertyChangedEventArgs("FirstTimeGuessesPercentage"));
            }
            // Go to switch offer
            this.OfferSwitch();
        }

        /// <summary>
        /// Open empty door and offer switch (Step 3)
        /// </summary>
        protected void OfferSwitch() {
            // Open empty door
            if (this._winningDoor != this._selectedDoor) {
                // Open only nonselected / nonwining door
                if ((this._winningDoor != 1) && (this._selectedDoor != 1)) this.IsDoor1Open = true;
                if ((this._winningDoor != 2) && (this._selectedDoor != 2)) this.IsDoor2Open = true;
                if ((this._winningDoor != 3) && (this._selectedDoor != 3)) this.IsDoor3Open = true;
            } else {
                // Open random nonselected / nonwining door
                int randomDoor = this.rand.Next(1, 4);
                if (this._selectedDoor == 1) {
                    if (randomDoor == 1) {
                        this.IsDoor2Open = true;
                    } else {
                        this.IsDoor3Open = true;
                    }
                } else if (this._selectedDoor == 2) {
                    if (randomDoor == 1) {
                        this.IsDoor1Open = true;
                    } else {
                        this.IsDoor3Open = true;
                    }
                } else {
                    if (randomDoor == 1) {
                        this.IsDoor1Open = true;
                    } else {
                        this.IsDoor2Open = true;
                    }
                }
            }
            // Prompt switch offer
            this.StatusTextbox.Text = "Do you wish to switch your choice?";
            // Switch door
            double timeout = 2123 / this.speedBar.Value;
            System.Timers.Timer timer = new System.Timers.Timer(timeout);
            timer.Elapsed += (sender, args) => {
                                 // Stop timer
                                 timer.Stop();
                                 // Auto choose door
                                 this.Invoke(new Action(this.SwitchDoor));
                             };
            timer.Start();
        }

        /// <summary>
        /// Switches selected door
        /// </summary>
        protected void SwitchDoor() {
            // Prompt switch offer's anwser
            bool persist = this.PersistSelection;
            this.StatusTextbox.Text = (persist ? "Do you wish to switch your choice? No!" : "Do you wish to switch your choice? Yes!");
            // Check if persistant choice
            if (!persist) {
                if ((this._selectedDoor != 1) && (!this.IsDoor1Open)) {
                    this.SelectedDoor = 1;
                } else  if ((this._selectedDoor != 2) && (!this.IsDoor2Open)) {
                    this.SelectedDoor = 2;
                } else if ((this._selectedDoor != 3) && (!this.IsDoor3Open)) {
                    this.SelectedDoor = 3;
                }
            }
            // Resolve quiz
            double timeout = 2123 / this.speedBar.Value;
            System.Timers.Timer timer = new System.Timers.Timer(timeout);
            timer.Elapsed += (sender, args) => {
                                 // Stop timer
                                 timer.Stop();
                                 // Auto choose door
                                 this.Invoke(new Action(this.ResolveQuiz));
                             };
            timer.Start();
        }

        /// <summary>
        /// Resolves quiz
        /// </summary>
        protected void ResolveQuiz() {
            // Open all dors
            this.IsDoor1Open = true;
            this.IsDoor2Open = true;
            this.IsDoor3Open = true;
            // Prompt win / loose
            this.StatusTextbox.Text = (this._winningDoor == this._selectedDoor ? "Win!" : "Fail!");
            // Update stats
            this.UpdateStats(this._winningDoor == this._selectedDoor);
            // Restart quiz
            double timeout = 4245 / this.speedBar.Value;
            System.Timers.Timer timer = new System.Timers.Timer(timeout);
            timer.Elapsed += (sender, args) => {
                                 // Stop timer
                                 timer.Stop();
                                 // Restart quiz
                                 this.Invoke(new Action(this.SetupChoice));
                             };
            timer.Start();
        }

        /// <summary>
        /// Updates winning / losing stats
        /// </summary>
        protected void UpdateStats(bool win) {
            // Update wins / loses
            if (win) {
                // Increse wins
                this._wins++;
            } else {
                // Increse loses
                this._loses++;
            }
            // Add stat point
            this._stats.Add((double)this._wins / ((double)this._wins + (double)this._loses));
            // Redraw stats
            this.OnPropertyChanged(new PropertyChangedEventArgs("Wins"));
            this.OnPropertyChanged(new PropertyChangedEventArgs("Loses"));
            this.OnPropertyChanged(new PropertyChangedEventArgs("WinsPercentage"));
            Image statsImage = new Bitmap(this.statsImagePanel.Width, 300);
            Graphics g = Graphics.FromImage(statsImage);
            int x = 0;
            foreach (double point in this._stats) {
                float w = (float)this.statsImagePanel.Width / (float)this._stats.Count;
                g.FillRectangle(Brushes.Coral, x * w, (float)0, w, (float)(300 * (1 - point)));
                g.FillRectangle(Brushes.GreenYellow, x * w, (float)(300 * (1 - point)), (float)Math.Ceiling(w), (float)(300 * point));
                x++;
            }
            g.Dispose();
            this.StatsImage = statsImage;
        }

        #endregion

        // =============================================================================================================================
        // User events
        // =============================================================================================================================
        #region User events

        /// <summary>
        /// On door clicked (Step 2.1)
        /// </summary>
        private void DoorPanel_Click(object sender, EventArgs e) {
            if (this._allowSelect) {
                if (sender == this.Door1SelectedPanel) this.ChooseDoor(1);
                if (sender == this.Door2SelectedPanel) this.ChooseDoor(2);
                if (sender == this.Door3SelectedPanel) this.ChooseDoor(3);
            }
        }

        /// <summary>
        /// On cheat check ckeched
        /// </summary>
        private void cheatCheck_CheckedChanged(object sender, EventArgs e) {
            this.OnPropertyChanged(new PropertyChangedEventArgs("Door1PrizeImage"));
            this.OnPropertyChanged(new PropertyChangedEventArgs("Door2PrizeImage"));
            this.OnPropertyChanged(new PropertyChangedEventArgs("Door3PrizeImage"));
        }

        /// <summary>
        /// Reset stats
        /// </summary>
        private void ResetStatsButton_Click(object sender, EventArgs e) {
            this._ftGuesses = 0;
            this._ftMisses = 0;
            this.OnPropertyChanged(new PropertyChangedEventArgs("FirstTimeGuesses"));
            this.OnPropertyChanged(new PropertyChangedEventArgs("FirstTimeMisses"));
            this.OnPropertyChanged(new PropertyChangedEventArgs("FirstTimeGuessesPercentage"));
            this._wins = 0;
            this._loses = 0;
            this.OnPropertyChanged(new PropertyChangedEventArgs("Wins"));
            this.OnPropertyChanged(new PropertyChangedEventArgs("Loses"));
            this.OnPropertyChanged(new PropertyChangedEventArgs("WinsPercentage"));
            this._stats = new List<double>();
            this.StatsImage = new Bitmap(1, 300);
        }

        /// <summary>
        /// On wikipedia link clicked
        /// </summary>
        private void wikiLink_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e) {
            // Open "Monty hall problem" article on wikipedia
            Process proc = new Process();
            proc.StartInfo.UseShellExecute = true;
            proc.StartInfo.FileName = "http://en.wikipedia.org/wiki/Monty_Hall_problem";
            proc.Start();
        }

        #endregion

    }

}
